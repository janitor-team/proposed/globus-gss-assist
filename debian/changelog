globus-gss-assist (12.7-2) unstable; urgency=medium

  * Make doxygen Build-Depends-Indep
  * Drop old debug symbol migration from 2017

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 05 Jul 2022 22:27:47 +0200

globus-gss-assist (12.7-1) unstable; urgency=medium

  * New GCT release v6.2.20220524
  * Drop patches included in the release

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 25 May 2022 22:13:22 +0200

globus-gss-assist (12.6-2) unstable; urgency=medium

  * Use sha256 hash when generating test certificates

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 13 May 2022 08:14:59 +0200

globus-gss-assist (12.6-1) unstable; urgency=medium

  * Typo fixes

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 27 Aug 2021 09:54:33 +0200

globus-gss-assist (12.5-1) unstable; urgency=medium

  * Minor bug fixes and code maintenance

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 17 Aug 2021 17:17:46 +0200

globus-gss-assist (12.4-1) unstable; urgency=medium

  * Minor fixes to makefiles (12.3)
  * Add force option to grid-mapfile-add-entry if the users do not exist (12.4)
  * Change to debhelper compat level 13
  * Remove override_dh_missing rule (--fail-missing is default)
  * Drop old symlink-to-dir conversion from 2014
  * Drop ancient Replaces/Conflicts/Breaks

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Tue, 15 Dec 2020 12:40:49 +0100

globus-gss-assist (12.2-2) unstable; urgency=medium

  * Convert debian/rules to dh tool
  * Change to debhelper compat level 10
  * Update documentation links in README file

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Fri, 12 Jul 2019 16:34:46 +0200

globus-gss-assist (12.2-1) unstable; urgency=medium

  * Doxygen fixes
  * Use .maintscript file for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Wed, 27 Feb 2019 20:00:58 +0100

globus-gss-assist (12.1-1) unstable; urgency=medium

  * Switch upstream to Grid Community Toolkit
  * First Grid Community Toolkit release (12.0)
  * Use 2048 bit RSA key for tests (12.1)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 16 Sep 2018 03:16:32 +0200

globus-gss-assist (11.2-1) unstable; urgency=medium

  * GT6 update: Use 2048 bit keys to support openssl 1.1.1
  * Drop patch globus-gss-assist-2048-bits.patch (acceoted upstream)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sat, 01 Sep 2018 20:57:50 +0200

globus-gss-assist (11.1-2) unstable; urgency=medium

  * Use 2048 bit RSA key for tests
  * Move VCS to salsa.debian.org

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 26 Aug 2018 13:51:53 +0200

globus-gss-assist (11.1-1) unstable; urgency=medium

  * GT6 update: race condition and dependency packaging fixes
  * Drop patches globus-gss-assist-deps.patch and globus-gss-assist-race.patch
    (accepted upstream)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 22 Oct 2017 17:16:09 +0200

globus-gss-assist (11.0-1) unstable; urgency=medium

  * GT6 update: Add new function gss_assist_read_vhost_cred_dir() for SNI
    server
  * Fix race condition in the Makefile in test directory

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sun, 10 Sep 2017 08:30:53 +0200

globus-gss-assist (10.21-2) unstable; urgency=medium

  * Migrate to dbgsym packages
  * Support DEB_BUILD_OPTIONS=nocheck

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 06 Jul 2017 12:58:35 +0200

globus-gss-assist (10.21-1) unstable; urgency=medium

  * GT6 update: Slow grid-mapfile-delete-entry (issue #84)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 12 Jan 2017 14:37:30 +0100

globus-gss-assist (10.20-1) unstable; urgency=medium

  * GT6 update: Updated man pages

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 10 Nov 2016 19:42:57 +0100

globus-gss-assist (10.19-1) unstable; urgency=medium

  * GT6 update: Fix grid-mapfile-add-entry is slow (issue #69)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Sat, 03 Sep 2016 15:51:04 +0200

globus-gss-assist (10.17-1) unstable; urgency=medium

  * GT6 update: Updates for OpenSSL 1.1.0
  * Change Maintainer e-mail (fysast → physics)

 -- Mattias Ellert <mattias.ellert@physics.uu.se>  Thu, 01 Sep 2016 14:45:54 +0200

globus-gss-assist (10.15-2) unstable; urgency=medium

  * Set SOURCE_DATE_EPOCH and rebuild using doxygen 1.8.11
    (for reproducible build)
  * Update URLs (use toolkit.globus.org)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 16 Feb 2016 16:03:26 +0100

globus-gss-assist (10.15-1) unstable; urgency=medium

  * GT6 update (Fix gridmap parsing error)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 29 Jul 2015 13:21:52 +0200

globus-gss-assist (10.14-1) unstable; urgency=medium

  * GT6 update (Fix uninitialized variable)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 12 Jul 2015 19:51:40 +0200

globus-gss-assist (10.13-2) unstable; urgency=medium

  * Rebuild using doxygen 1.8.9.1 (Closes: #630063)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 30 Apr 2015 14:26:42 +0200

globus-gss-assist (10.13-1) unstable; urgency=medium

  * GT6 update
  * Drop patch globus-gss-assist-doxygen.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 15 Nov 2014 13:43:02 +0100

globus-gss-assist (10.12-3) unstable; urgency=medium

  * Add Pre-Depends for dpkg-maintscript-helper

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 08 Nov 2014 23:22:04 +0100

globus-gss-assist (10.12-2) unstable; urgency=medium

  * Properly handle symlink-to-dir conversion in doc package
  * Enable verbose tests

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 07 Nov 2014 17:15:00 +0100

globus-gss-assist (10.12-1) unstable; urgency=medium

  * GT6 update
  * Update patch globus-gss-assist-doxygen.patch

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 27 Oct 2014 10:43:36 +0100

globus-gss-assist (10.11-1) unstable; urgency=medium

  * Update to Globus Toolkit 6.0
  * Drop GPT build system and GPT packaging metadata
  * Enable checks

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 23 Sep 2014 14:43:01 +0200

globus-gss-assist (9.0-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.5
  * Implement Multi-Arch support
  * Rename dbg package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 10 Nov 2013 16:39:11 +0100

globus-gss-assist (8.7-2) unstable; urgency=low

  * Add arm64 to the list of 64 bit architectures

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 26 May 2013 17:23:48 +0200

globus-gss-assist (8.7-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.4

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 20 Feb 2013 13:56:18 +0100

globus-gss-assist (8.6-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.2
  * Drop patch globus-gss-assist-mingw.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 23 Jul 2012 11:51:04 +0200

globus-gss-assist (8.5-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.1
  * Drop patches globus-gss-assist-doxygen.patch, globus-gss-assist-deps.patch
    and globus-gss-assist-format.patch (fixed upstream)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sun, 29 Apr 2012 07:03:12 +0200

globus-gss-assist (8.1-2) unstable; urgency=low

  * Fix broken links in README file

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 24 Jan 2012 18:16:13 +0100

globus-gss-assist (8.1-1) unstable; urgency=low

  * Update to Globus Toolkit 5.2.0
  * Drop patches globus-gss-assist-normalization.patch and
    globus-gss-assist-gridmapdir.patch (fixed upstream)
  * Make doc package architecture independent

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 28 Dec 2011 08:24:39 +0100

globus-gss-assist (5.10-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.4
  * Fix doxygen markup
  * Use system jquery script

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 06 Jun 2011 09:55:57 +0200

globus-gss-assist (5.9-2) unstable; urgency=low

  * Add README file
  * Use new doxygen-latex build dependency (Closes: #616241)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Apr 2011 08:54:28 +0200

globus-gss-assist (5.9-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.2
  * Move client man pages to progs package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Mon, 19 Jul 2010 08:56:35 +0200

globus-gss-assist (5.8-2) unstable; urgency=low

  * Converting to package format 3.0 (quilt)
  * Add new build dependency on texlive-font-utils due to changes in texlive
    packaging (epstopdf moved there) (Closes: #583038)

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Fri, 04 Jun 2010 04:43:30 +0200

globus-gss-assist (5.8-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.1

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 15 Apr 2010 14:37:20 +0200

globus-gss-assist (5.5-1) unstable; urgency=low

  * Update to Globus Toolkit 5.0.0
  * Add debug package

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Tue, 26 Jan 2010 19:00:40 +0100

globus-gss-assist (4.0-4) unstable; urgency=low

  * Fix rule dependencies in the debian/rules file.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Wed, 13 May 2009 14:07:54 +0200

globus-gss-assist (4.0-3) unstable; urgency=low

  * Initial release (Closes: #514472).
  * Rebuilt to correct libltdl dependency.
  * Preparing for other 64bit platforms than amd64.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 18 Apr 2009 20:17:33 +0200

globus-gss-assist (4.0-2) UNRELEASED; urgency=low

  * Only quote the Apache-2.0 license if necessary.
  * Updated deprecated Source-Version in debian/control.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Thu, 26 Mar 2009 09:21:25 +0100

globus-gss-assist (4.0-1) UNRELEASED; urgency=low

  * First build.

 -- Mattias Ellert <mattias.ellert@fysast.uu.se>  Sat, 03 Jan 2009 10:18:08 +0100
